package net.techu.hackathon.grupo8.controllers;


import net.techu.hackathon.grupo8.models.Tarjeta;
import net.techu.hackathon.grupo8.service.TarjetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hackaton/v1/clientes")
public class TarjetaController {
    private final TarjetaService tarjetaService;
    private Tarjeta tarjeta;

    @Autowired
    public TarjetaController(TarjetaService tarjetaService) {
        this.tarjetaService = tarjetaService;
    }

    @GetMapping("/tarjetas")
    public ResponseEntity<List<Tarjeta>> allTarjetas() {
        System.out.println("Me piden las tarjetas");
        return ResponseEntity.ok(tarjetaService.findAll());
    };

    @GetMapping("/{customer_id}/tarjetas")
    public ResponseEntity<List<Tarjeta>> oneTarjetas(@PathVariable("customer_id") Integer customer_id) {
        System.out.println("Me piden las tarjetas del customer_id");
        return ResponseEntity.ok(tarjetaService.findTarjetasCliente(customer_id));
    };


}

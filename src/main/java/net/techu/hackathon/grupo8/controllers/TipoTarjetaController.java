package net.techu.hackathon.grupo8.controllers;

import net.techu.hackathon.grupo8.models.CategoriaTarjeta;
import net.techu.hackathon.grupo8.models.TipoTarjeta;
import net.techu.hackathon.grupo8.service.CategoriaTarjetaService;
import net.techu.hackathon.grupo8.service.TipoTarjetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hackaton/v1/tipotarjetas")
public class TipoTarjetaController {
    private final TipoTarjetaService tipoTarjetaService;
    private CategoriaTarjeta categoriaTarjeta;

    @Autowired
    public TipoTarjetaController(TipoTarjetaService tipoTarjetaService) { this.tipoTarjetaService = tipoTarjetaService; }

    @GetMapping()
    public ResponseEntity<List<TipoTarjeta>> tipoTarjetas()
    {
        return ResponseEntity.ok(tipoTarjetaService.findAll());
    }

    @PostMapping()
    public ResponseEntity<TipoTarjeta> saveTipoTarjeta(@RequestBody TipoTarjeta tipoTarjeta)
    {
        return ResponseEntity.ok(tipoTarjetaService.saveTipoTarjeta(tipoTarjeta));
    }
}

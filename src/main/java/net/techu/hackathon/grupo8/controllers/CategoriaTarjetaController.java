package net.techu.hackathon.grupo8.controllers;

import net.techu.hackathon.grupo8.models.CategoriaTarjeta;
import net.techu.hackathon.grupo8.service.CategoriaTarjetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hackaton/v2/categoriatarjetas")
public class CategoriaTarjetaController {
    private final CategoriaTarjetaService categoriaTarjetaService;
    private CategoriaTarjeta categoriaTarjeta;

    @Autowired
    public CategoriaTarjetaController(CategoriaTarjetaService categoriaTarjetaService) {
        this.categoriaTarjetaService = categoriaTarjetaService;
    }

    @GetMapping()
    public ResponseEntity<List<CategoriaTarjeta>> categoriaTarjetas() {
        return ResponseEntity.ok(categoriaTarjetaService.findAll());
    }

    @PostMapping()

    public ResponseEntity<CategoriaTarjeta> saveCategoriaTarjeta(@RequestBody CategoriaTarjeta categoriaTarjeta) {
        return ResponseEntity.ok(categoriaTarjetaService.saveCategoriaTarjeta(categoriaTarjeta));
    }
}

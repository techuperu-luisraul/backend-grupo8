package net.techu.hackathon.grupo8.repository;

import net.techu.hackathon.grupo8.models.CategoriaTarjeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoriaTarjetaRepositoryImpl implements CategoriaTarjetaRepository {
    private final MongoOperations mongoOperations;

    @Autowired
    public CategoriaTarjetaRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<CategoriaTarjeta> findAll() {
        List<CategoriaTarjeta> categoriaTarjetas = this.mongoOperations.find(new Query(), CategoriaTarjeta.class);
        return categoriaTarjetas;
    }

    @Override
    public CategoriaTarjeta findOne(String id) {
        CategoriaTarjeta encontrado = this.mongoOperations.findOne(new Query(Criteria.where("category_id").is(id)),CategoriaTarjeta.class);
        return encontrado;
    }

    @Override
    public CategoriaTarjeta saveCategoriaTarjeta(CategoriaTarjeta categoriaTarjeta) {
        this.mongoOperations.save(categoriaTarjeta);
        return findOne(categoriaTarjeta.getCategory_id());
    }

    @Override
    public void updateCategoriaTarjeta(CategoriaTarjeta categoriaTarjeta) {
        this.mongoOperations.save(categoriaTarjeta);
    }

    @Override
    public void deleteCategoriaTarjeta(String id) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("category_id").is(id)), CategoriaTarjeta.class);
    }
}

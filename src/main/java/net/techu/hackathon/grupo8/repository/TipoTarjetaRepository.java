package net.techu.hackathon.grupo8.repository;

import net.techu.hackathon.grupo8.models.TipoTarjeta;

import java.util.List;

public interface TipoTarjetaRepository {
    List<TipoTarjeta> findAll();
    public TipoTarjeta findOne(String id);
    public TipoTarjeta saveTipoTarjeta(TipoTarjeta client);
    public void updateTipoTarjeta(TipoTarjeta client);
    public void deleteTipoTarjeta(String id);
}

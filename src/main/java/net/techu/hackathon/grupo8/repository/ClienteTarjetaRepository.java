package net.techu.hackathon.grupo8.repository;

import net.techu.hackathon.grupo8.models.ClienteTarjetaModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteTarjetaRepository extends MongoRepository<ClienteTarjetaModel, String> {
}

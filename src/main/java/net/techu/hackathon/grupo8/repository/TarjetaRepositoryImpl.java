package net.techu.hackathon.grupo8.repository;

import net.techu.hackathon.grupo8.models.Tarjeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TarjetaRepositoryImpl implements TarjetaRepository {

    private final MongoOperations mongoOperations;

    @Autowired
    public TarjetaRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Tarjeta> findAll() {
        List<Tarjeta> tarjetas = this.mongoOperations.find(new Query(), Tarjeta.class);
        return tarjetas;
    }

        @Override
    public List<Tarjeta> findTarjetasCliente(Integer customer_id) {
        List<Tarjeta> encounter = this.mongoOperations.find(new Query(Criteria.where("customer_id").is(customer_id)),Tarjeta.class);
        return encounter;
    }

}

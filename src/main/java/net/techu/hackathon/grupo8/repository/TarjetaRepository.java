package net.techu.hackathon.grupo8.repository;

import net.techu.hackathon.grupo8.models.Tarjeta;

import java.util.List;

public interface TarjetaRepository {
    List<Tarjeta> findAll();

    public List<Tarjeta> findTarjetasCliente(Integer customer_id);

}

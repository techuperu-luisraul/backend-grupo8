package net.techu.hackathon.grupo8.repository;

import net.techu.hackathon.grupo8.models.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ClienteRepositoryImpl implements ClienteRepository {
    private final MongoOperations mongoOperations;

    @Autowired
    public ClienteRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Cliente> findAll() {
        List<Cliente> clientes = this.mongoOperations.find(new Query(), Cliente.class);
        return clientes;
    }

    @Override
    public Cliente findOne(String id) {
        Cliente encontrado = this.mongoOperations.findOne(new Query(Criteria.where("customer_id").is(id)),Cliente.class);
        return encontrado;
    }

    @Override
    public Cliente saveCliente(Cliente client) {
        this.mongoOperations.save(client);
        return findOne(client.getCustomer_id());
    }

    @Override
    public void updateCliente(Cliente client) {
        this.mongoOperations.save(client);
    }

    @Override
    public void deleteCliente(String id) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("customer_id").is(id)), Cliente.class);
    }
}

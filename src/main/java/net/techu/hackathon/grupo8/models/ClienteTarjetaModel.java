package net.techu.hackathon.grupo8.models;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "cliente_tarjeta")
public class ClienteTarjetaModel {
    @Id
    @NotNull
    private String id;
    private String cliente;
    private String tipo_documento;
    private String nro_documento;
    private List<TarjetaModel> tarjetas;

    public ClienteTarjetaModel() {
    }

    ;

    public ClienteTarjetaModel(String id, String cliente, String tipo_documento, String nro_documento) {
        this.id = id;
        this.cliente = cliente;
        this.tipo_documento = tipo_documento;
        this.nro_documento = nro_documento;
    }

    public ClienteTarjetaModel(String id, String cliente, String tipo_documento, String nro_documento, List<TarjetaModel> tarjetas) {
        this.id = id;
        this.cliente = cliente;
        this.tipo_documento = tipo_documento;
        this.nro_documento = nro_documento;
        this.tarjetas = tarjetas;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getNro_documento() {
        return nro_documento;
    }

    public void setNro_documento(String nro_documento) {
        this.nro_documento = nro_documento;
    }

    public List<TarjetaModel> getTarjetas() {
        return tarjetas;
    }

    public void setTarjetas(List<TarjetaModel> tarjetas) {
        this.tarjetas = tarjetas;
    }

}

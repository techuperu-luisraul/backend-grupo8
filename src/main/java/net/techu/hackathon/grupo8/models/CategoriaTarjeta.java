package net.techu.hackathon.grupo8.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "categoriastarjeta")
@JsonPropertyOrder({"id","category_id", "description"})
public class CategoriaTarjeta {
    @Id
    @NotNull
    private String id;
    @NotNull
    private String category_id;
    @NotNull
    private String description;

    public CategoriaTarjeta() {}

    public CategoriaTarjeta(String id, String category_id, String description)
    {
        this.setId(id);
        this.setCategory_id(category_id);
        this.setDescription(description);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

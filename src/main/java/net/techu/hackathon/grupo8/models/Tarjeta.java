package net.techu.hackathon.grupo8.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document("tarjetas")
@JsonPropertyOrder({"customer_id", "card_id", "card_type", "category_type", "card_number", "security_code", "limit", "card_active_flag"})
public class Tarjeta implements Serializable {

    @NotNull
    private Integer customer_id;
    @NotNull
    private String card_type;
    @NotNull
    private String category_type;
    @NotNull
    private String card_number;
    @NotNull
    private Integer security_code;
    @NotNull
    private String card_active_flag;

    private Integer card_id;
    private Integer limit;

    public Tarjeta() {
    }

    public Tarjeta(Integer customer_id, Integer card_id, String card_type, String category_type, Integer security_code, Integer limit, String card_active_flag) {
        this.setCustomer_id(customer_id);
        this.setCard_type(card_type);
        this.setCategory_type(category_type);
        this.getSecurity_code(security_code);
        this.setCard_active_flag(card_active_flag);
        this.setCard_id(card_id);
        this.setLimit(limit);

    }

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    public String getCategory_type() {
        return category_type;
    }

    public void setCategory_type(String category_type) {
        this.category_type = category_type;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public Integer getSecurity_code(Integer security_code) {
        return this.security_code;
    }

    public void setSecurity_code(Integer security_code) {
        this.security_code = security_code;
    }

    public String getCard_active_flag() {
        return card_active_flag;
    }

    public void setCard_active_flag(String card_active_flag) {
        this.card_active_flag = card_active_flag;
    }

    public Integer getCard_id() {
        return card_id;
    }

    public void setCard_id(Integer card_id) {
        this.card_id = card_id;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}

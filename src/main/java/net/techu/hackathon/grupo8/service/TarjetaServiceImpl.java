package net.techu.hackathon.grupo8.service;

import net.techu.hackathon.grupo8.models.Tarjeta;
import net.techu.hackathon.grupo8.repository.TarjetaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("TarjetaService")
@Transactional
public class TarjetaServiceImpl implements TarjetaService{

    private final TarjetaRepository tarjetaRepository;

    public TarjetaServiceImpl(TarjetaRepository tarjetaRepository) {
        this.tarjetaRepository = tarjetaRepository;
    }


    @Override
    public List<Tarjeta> findAll() {
        return tarjetaRepository.findAll();
    }

    @Override
    public List<Tarjeta> findTarjetasCliente(Integer customer_id) {
        return tarjetaRepository.findTarjetasCliente(customer_id);
    }
}

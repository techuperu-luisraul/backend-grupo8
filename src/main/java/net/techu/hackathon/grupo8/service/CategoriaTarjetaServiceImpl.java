package net.techu.hackathon.grupo8.service;

import net.techu.hackathon.grupo8.models.CategoriaTarjeta;
import net.techu.hackathon.grupo8.repository.CategoriaTarjetaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("categoriaTarjetaService")
@Transactional
public class CategoriaTarjetaServiceImpl implements CategoriaTarjetaService {

    private CategoriaTarjetaRepository categoriaTarjetaRepository;

    @Autowired
    public CategoriaTarjetaServiceImpl(CategoriaTarjetaRepository categoriaTarjetaRepository)
    {
        this.categoriaTarjetaRepository = categoriaTarjetaRepository;
    }

    @Override
    public List<CategoriaTarjeta> findAll() {
        return categoriaTarjetaRepository.findAll();
    }

    @Override
    public CategoriaTarjeta findOne(String id) {
        return categoriaTarjetaRepository.findOne(id);
    }

    @Override
    public CategoriaTarjeta saveCategoriaTarjeta(CategoriaTarjeta veh) { return categoriaTarjetaRepository.saveCategoriaTarjeta(veh); }

    @Override
    public void updateCategoriaTarjeta(CategoriaTarjeta veh) { categoriaTarjetaRepository.updateCategoriaTarjeta(veh); }

    @Override
    public void deleteCategoriaTarjeta(String id) { categoriaTarjetaRepository.deleteCategoriaTarjeta(id); }
}

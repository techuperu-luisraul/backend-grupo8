package net.techu.hackathon.grupo8.service;

import net.techu.hackathon.grupo8.models.CategoriaTarjeta;
import net.techu.hackathon.grupo8.models.TipoTarjeta;

import java.util.List;

public interface TipoTarjetaService {
    List<TipoTarjeta> findAll();
    public TipoTarjeta findOne(String id);
    public TipoTarjeta saveTipoTarjeta(TipoTarjeta veh);
    public void updateTipoTarjeta(TipoTarjeta veh);
    public void deleteTipoTarjeta(String id);
}

package net.techu.hackathon.grupo8.service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import net.techu.hackathon.grupo8.models.ClienteTarjetaModel;
import net.techu.hackathon.grupo8.models.TarjetaModel;
import net.techu.hackathon.grupo8.repository.ClienteTarjetaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class ClienteTarjetaService {
    private final MongoOperations mongoOperations;

    @Autowired
    ClienteTarjetaRepository clienteTarjetaRepository;

    @Autowired
    protected MongoTemplate mongoTemplate;

    public ClienteTarjetaService(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    public List<ClienteTarjetaModel> findAll() {
        return clienteTarjetaRepository.findAll();
    }

    public Optional<ClienteTarjetaModel> findById(String id) {
        return clienteTarjetaRepository.findById(id);
    }

    public ClienteTarjetaModel save(ClienteTarjetaModel entity) {
        return clienteTarjetaRepository.save(entity);
    }

    public boolean delete(ClienteTarjetaModel entity) {
        try {
            clienteTarjetaRepository.delete(entity);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public ClienteTarjetaModel insertTarjeta(String cliente, TarjetaModel tarjetaModel) {

        Query query = new Query();
        query.addCriteria(Criteria.where("cliente").is(cliente));
        Update update = new Update();
        update.push("tarjetas", tarjetaModel);
        return mongoOperations.findAndModify(query, update, ClienteTarjetaModel.class);

    }

}
